LiccBot 6900
============

Put random notes to the rhythm of [the Licc](https://en.wikipedia.org/wiki/The_Lick).
Facebook API shenanigans not (yet) included.

Visit the Page: [@liccbot](https://www.facebook.com/liccbot)

Dependencies
------------

* Perl 5.28.2
* [LilyPond](http://lilypond.org/)


Usage
-----

Change the variables at the top of the file to correspond to your
LilyPond binary and template files.
Change the PWD to the directory in which `liccbot.pl` exists, make a directory
called `liccs` (if it doesn't already exist), and run `./liccbot.pl`.

With default settings, there's a 1:78124 chance the bot will actually generate
the Licc (5 notes available for 7 positions: 5<sup>7</sup> possible combinations).

Contributing
------------

Perl is unreadable the way it is, so I don't particularly care how you format
your code as long as it's got tabs for indentation.
