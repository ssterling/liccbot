#!/usr/local/bin/perl
#
# LiccBot 6900 – put random notes to the rhythm of the Licc
# Copyright 2020 Seth Price, all rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#

use strict;
use warnings;
use 5.28.2;

use File::Temp qw(tempdir);
use File::Copy qw(move);
use Cwd;

#################################################
# LICC GENERATION

# Executable paths, etc.
my $lilypond_loc = '/usr/local/bin/lilypond';
my $liccbot_dir_loc = getcwd;
my $template_loc = "$liccbot_dir_loc/template.ly";

# Notes to choose from (absolute pitch)
my @notes = ("c'", "d'", "e'", "f'", "g'");
sub get_random_note { return $notes[rand @notes]; }

# Generate licc
my $tie_note = get_random_note;
my $licc = get_random_note . "8 " .
           get_random_note . " " .
           get_random_note . " " .
           get_random_note . " " .
           get_random_note . "4 " .
           get_random_note . "8 " .
           $tie_note . "~ | " .
           $tie_note . "1 |";

# Load template file
undef $/; # ignore newlines for my own sanity
open TEMPLATE, $template_loc or die "could not open file at `$template_loc'";
my $output_source = <TEMPLATE>;
close TEMPLATE or warn "could not close file at `$template_loc'";

# Replace placeholder in template with the generated licc
$output_source =~ s/%LICC GOES HERE%/$licc/;

# Generate timestamp for files
my $timestamp = time;

# Create temporary directory
my $temp_dir = tempdir (CLEANUP => 1); # TODO: error catching

# Write licc to temporary file
my $output_ly_loc = "$temp_dir/licc-$timestamp.ly";
open OUTPUT_LY, '>', $output_ly_loc
	or die "could not open file at `$output_ly_loc'";
print OUTPUT_LY "$output_source";
close OUTPUT_LY or warn "could not close file at `$output_ly_loc'";

# Compile it all
my $output_png_loc = "$temp_dir/output"; # no extension
my $lilypond_cmd = "$lilypond_loc -o'$output_png_loc' -fpng -dresolution=600 '$output_ly_loc'";
system "$lilypond_cmd";
die "failed to generate output png at `$output_png_loc'" if $? ne 0;

# Copy PNG file to directory
my $new_output_png_loc = "$liccbot_dir_loc/liccs/licc-$timestamp.png";
move $output_png_loc . ".png", $new_output_png_loc;

# Determine whether generated licc is THE licc
my $is_licc = 0;
$is_licc = 1 if $licc eq "d'8 e' f' g' e'4 c'8 d'~ | d'1 |";

0;
