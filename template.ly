% Template file for LiccBot 6900

\version "2.18.2"

% Remove tagline
\header { tagline = "" }

% Set PNG to just the right size
\paper
{
	paper-height = 2.7\cm
	paper-width = 8.8\cm
	margin = 0\cm
}

\score
{
	\new Staff
	{
		\clef treble
		\key d \minor
		\time 4/4

		%LICC GOES HERE%

		\bar "|."
	}

	\layout
	{
		indent = 0\cm

		% Use better music fonts
		%\context { \Staff \bravuraOn }
	}
}
